set(_CATKIN_CURRENT_PACKAGE "motion_planning_pkg")
set(motion_planning_pkg_VERSION "0.0.0")
set(motion_planning_pkg_MAINTAINER "ncta <ncta@todo.todo>")
set(motion_planning_pkg_PACKAGE_FORMAT "2")
set(motion_planning_pkg_BUILD_DEPENDS "roscpp" "rospy" "std_msgs" "ackermann_msgs" "geometry_msgs" "nav_msgs" "sensor_msgs" "tf" "visualization_msgs" "tf2_ros")
set(motion_planning_pkg_BUILD_EXPORT_DEPENDS "roscpp" "rospy" "std_msgs")
set(motion_planning_pkg_BUILDTOOL_DEPENDS "catkin")
set(motion_planning_pkg_BUILDTOOL_EXPORT_DEPENDS )
set(motion_planning_pkg_EXEC_DEPENDS "roscpp" "rospy" "std_msgs" "ackermann_msgs" "geometry_msgs" "nav_msgs" "sensor_msgs" "tf" "visualization_msgs" "tf2_ros")
set(motion_planning_pkg_RUN_DEPENDS "roscpp" "rospy" "std_msgs" "ackermann_msgs" "geometry_msgs" "nav_msgs" "sensor_msgs" "tf" "visualization_msgs" "tf2_ros")
set(motion_planning_pkg_TEST_DEPENDS )
set(motion_planning_pkg_DOC_DEPENDS )
set(motion_planning_pkg_URL_WEBSITE "")
set(motion_planning_pkg_URL_BUGTRACKER "")
set(motion_planning_pkg_URL_REPOSITORY "")
set(motion_planning_pkg_DEPRECATED "")