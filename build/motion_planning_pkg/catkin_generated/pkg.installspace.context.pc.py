# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "${prefix}/include".split(';') if "${prefix}/include" != "" else []
PROJECT_CATKIN_DEPENDS = "ackermann_msgs;geometry_msgs;roscpp;rospy;sensor_msgs;std_msgs;tf;tf2_ros;visualization_msgs".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-lmotion_planning_pkg".split(';') if "-lmotion_planning_pkg" != "" else []
PROJECT_NAME = "motion_planning_pkg"
PROJECT_SPACE_DIR = "/home/ncta/tienanh_ws/install"
PROJECT_VERSION = "0.0.0"
