# CMake generated Testfile for 
# Source directory: /home/ncta/tienanh_ws/src
# Build directory: /home/ncta/tienanh_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("scan_matching_pkg")
subdirs("beginner_tutorials")
subdirs("pure_pursuit_pkg")
subdirs("motion_planning_pkg")
subdirs("f1tenth_simulator")
