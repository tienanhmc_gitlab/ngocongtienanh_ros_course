// #include "beginner_tutorials/utility.h"


#include <ackermann_msgs/AckermannDriveStamped.h>
#include "ros/ros.h"
#include <sensor_msgs/LaserScan.h>
#include <iostream>
#include <vector>

#include <math.h>

namespace fgm
{


std::pair<size_t, size_t> truncated_start_and_end_indices(const sensor_msgs::LaserScan::ConstPtr &scan_msg,
        const double truncation_angle_coverage)
{
    const auto truncated_range_size = static_cast<size_t >(
            (truncation_angle_coverage/(scan_msg->angle_max - scan_msg->angle_min))*scan_msg->ranges.size());
    const size_t start_index = (scan_msg->ranges.size()/2) - (truncated_range_size/2);
    const size_t end_index = (scan_msg->ranges.size()/2) + (truncated_range_size/2);
    return {start_index, end_index};
}

size_t minimum_element_index(const std::vector<double>& input_vector)
{
    const auto min_value_iterator = std::min_element(input_vector.begin(), input_vector.end());
    return std::distance(input_vector.begin(), min_value_iterator);
}

size_t maximum_element_index(const std::vector<double>& input_vector)
{
    const auto max_value_iterator = std::max_element(input_vector.begin(), input_vector.end());
    return std::distance(input_vector.begin(), max_value_iterator);
}

std::pair<size_t, size_t> fine_max_gap(const std::vector<double>& input_vector, double look_foward_distance)
{   
    size_t curr_max_gap = 0;
    size_t max_gap = 0;
    size_t start_index = 0;
    size_t end_index = 0;
    std::vector<double> sub_vector = input_vector;
    sub_vector[input_vector.size() - 1] = 0.0;
    for (size_t i = 0; i<=input_vector.size()-1;i++){
        if (sub_vector[i] == look_foward_distance){
            curr_max_gap++;
        }
        else{
            if(curr_max_gap > max_gap){
                max_gap = curr_max_gap;
                end_index = i - 1;
            }
            curr_max_gap = 0;
        }
    }
    start_index = end_index - max_gap;
    return {start_index, end_index};

}


void zero_out_safety_bubble(std::vector<double>* input_vector, const size_t closest_point_idx,
                            const double bubble_radius)
{
    const double closest_distance =  input_vector->at(closest_point_idx);
    // input_vector->at(closest_point_idx) = 0.0;
    double anpha;
    anpha = atan(bubble_radius/closest_distance);
    int N; //number of element of ranges belong to the bubble
    N = int(anpha*343);
    // N = int(2*anpha/((360*3.14)/(180*1080))); //need to general recipe

    for (int i = 0;i <= N; i++){
        if (i < input_vector->size())
        {
            input_vector->at(i) = 0.0;
        }
        else break;
    
    }
    // size_t current_index = closest_point_idx;
    // while((current_index < input_vector->size()-1) &&
    //         (input_vector->at(++current_index) < (closest_distance + bubble_radius)))
    // {
    //     input_vector->at(current_index) = 0.0;
    // }

    // current_index = closest_point_idx;
    // while(current_index > 0 &&
    //         (input_vector->at(--current_index)) < (closest_distance + bubble_radius))
    // {
    //     input_vector->at(current_index) = 0.0;
    // }
}

}

/// implement the follow the gap method
class follow_the_gap
{
public:
    follow_the_gap() :
            node_handle_(ros::NodeHandle()),
            lidar_sub_(node_handle_.subscribe("scan", 100, &follow_the_gap::scan_callback, this)),
            drive_pub_(node_handle_.advertise<ackermann_msgs::AckermannDriveStamped>("nav", 100))
    {
        bubble_radius_ = 0.2;  //0.2
        truncated_coverage_angle_ = 2.618; //2.618
        velocity_ = 1.5; //max velocity  //4.0
        look_foward_distance = 1.5;    //1.8
    }

    std::vector<double> preprocess_lidar_scan(const sensor_msgs::LaserScan::ConstPtr &scan_msg) const
    {
        std::vector<double> filtered_ranges;
        for(size_t i=truncated_start_index_; i<truncated_end_index_; ++i)
        {
            if(std::isnan(scan_msg->ranges[i]))
            {
                filtered_ranges.push_back(0.0);
            }
            else if(scan_msg->ranges[i] > look_foward_distance || std::isinf(scan_msg->ranges[i]))
            {
                filtered_ranges.push_back(look_foward_distance);
            }
            else
            {
                filtered_ranges.push_back(scan_msg->ranges[i]);
            }
        }
        return filtered_ranges;
    }

    size_t get_best_point(const std::vector<double>& filtered_ranges, int start_index, int end_index) const
    {
        return (start_index + end_index)/2;
    }


    double get_steering_angle_from_range_index(const sensor_msgs::LaserScan::ConstPtr &scan_msg,
                                               const size_t best_point_index,
                                               const double closest_value)
    {
        const size_t best_point_index_input_scan_frame = truncated_start_index_ + best_point_index;
        double best_point_steering_angle;
        // Case When Steering Angle is to be negative
        if(best_point_index_input_scan_frame < scan_msg->ranges.size()/2)
        {
            best_point_steering_angle = - scan_msg->angle_increment*
                               static_cast<double>(scan_msg->ranges.size()/2.0 - best_point_index_input_scan_frame);
        }
            // Case When Steering Angle is to be negative
        else
        {
            best_point_steering_angle = scan_msg->angle_increment*
                               static_cast<double>(best_point_index_input_scan_frame - scan_msg->ranges.size()/2.0);
        }
 
        return best_point_steering_angle;
    }

    void scan_callback(const sensor_msgs::LaserScan::ConstPtr &scan_msg)
    {

            const auto truncated_indices =
                    fgm::truncated_start_and_end_indices(scan_msg, truncated_coverage_angle_);
            truncated_start_index_ = truncated_indices.first;
            truncated_end_index_ = truncated_indices.second;

        // // Pre-Process (zero out nans and Filter)
        auto filtered_ranges = preprocess_lidar_scan(scan_msg);

        // // find the closest point to LiDAR
        const size_t closest_index = fgm::minimum_element_index(filtered_ranges);

        const auto closest_range = filtered_ranges[closest_index];

        // // Eliminate all points inside 'bubble' (set them to zero)
        fgm::zero_out_safety_bubble(&filtered_ranges, closest_index, bubble_radius_);

        // // Find max length gap
        const auto gap_idx = fgm::fine_max_gap(filtered_ranges, look_foward_distance);
        size_t start_index = gap_idx.first;
        size_t end_index = gap_idx.second;
        // ROS_INFO("Max Gap Start Index = %u", static_cast<u_int>(start_index + truncated_start_index_));
        // ROS_INFO("Max Gap End Index = %u", static_cast<u_int>(end_index + truncated_start_index_));
       
        // // Find the best point in the gap
        const size_t best_point_index = get_best_point(filtered_ranges, start_index, end_index);
    //    const size_t best_point_index= fgm::maximum_element_index(filtered_ranges);

        const double steering_angle = get_steering_angle_from_range_index(scan_msg, best_point_index, closest_range);


     
        // velocity_ = 3 - steering_angle*2.29;
        // if (closest_range > 0.9){  
        //     velocity_ = 10000.0;
        // }
        // else velocity_ = 4.0;
       
        
        // Publish Drive message
        ackermann_msgs::AckermannDriveStamped drive_msg;
        drive_msg.header.stamp = ros::Time::now();
        drive_msg.header.frame_id = "laser";
        drive_msg.drive.speed = velocity_;
        drive_msg.drive.steering_angle = steering_angle;
        drive_pub_.publish(drive_msg);
        
    }

private:
    ros::NodeHandle node_handle_;
    ros::Subscriber lidar_sub_;
    ros::Publisher drive_pub_;

    double bubble_radius_;
    double look_foward_distance;
    double smoothing_filter_size_;
    double steering_angle_reactivity_;

    double truncated_coverage_angle_;
    size_t truncated_start_index_;
    size_t truncated_end_index_;

    double velocity_;

    std::map<std::string, double> error_based_velocities_;
};

int main(int argc, char ** argv)
{
    ros::init(argc, argv, "follow_the_gap_node");
    follow_the_gap gap_follower;
    ros::spin();
    return 0;
}