#!/usr/bin/env python
from __future__ import print_function
from os import kill
import sys
import math
import numpy as np

#ROS Imports
import rospy
from sensor_msgs.msg import Image, LaserScan
from ackermann_msgs.msg import AckermannDriveStamped, AckermannDrive

#bubble's radius
rb = 0.25    #0.55

max_steering_angle = 20
look_ahead_distance = 1.8  #1.5   #3.0

class reactive_follow_gap:
    def __init__(self):
        #Topics & Subscriptions,Publishers
        lidarscan_topic = '/scan'
        drive_topic = '/nav'

        self.lidar_sub = rospy.Subscriber("scan", LaserScan, self.lidar_callback) #TODO
        self.drive_pub = rospy.Publisher("nav", AckermannDriveStamped, queue_size=10) #TODO
    
    def preprocess_lidar(self, ranges):
        """ Preprocess the LiDAR scan array. Expert implementation includes:
            1.Setting each value to the mean over some window
            2.Rejecting high values (eg. > 3m)
        """
        # print(len(ranges)-2)
        ranges[0] = (ranges[0] + ranges[1])/2
        ranges[len(ranges)-1] = (ranges[len(ranges)-1] + ranges[len(ranges)-2])/2
        for i in range(1, len(ranges)-2):
            sum = ranges[i-1] + ranges[i] + ranges[i+1]
            ranges[i] = sum/3
        for i in range(len(ranges)):
            if (ranges[i] > look_ahead_distance):
                ranges[i] = look_ahead_distance
        # for i in range(len(ranges)):
        #     if (i < 15*4) or (i > 1080-15*4):
        #         ranges[i] = 0
        # print(ranges)
        proc_ranges = ranges
        return proc_ranges

    def find_max_gap(self, free_space_ranges):
        """ Return the start index & end index of the max gap in free_space_ranges
        """
        max_len = 0
        count = 0
        start_index = 0
        end_index = 0
        for i in range(4*105,1080-4*105): #len(free_space_ranges)):
            if free_space_ranges[i] != look_ahead_distance:
                count = 0
            else:
                count += 1
                if max_len < count:
                    end_index = i
                    max_len = max(max_len, count)
        start_index = end_index - max_len + 1
        return start_index, end_index
    
    def find_best_point(self, start_i, end_i, ranges):
        """Start_i & end_i are start and end indicies of max-gap range, respectively
        Return index of best point in ranges
	Naive: Choose the furthest point within ranges and go there
        """
        # max_range = 0.0
        # for i in range(start_i, end_i):
        #     if max_range < ranges[i]:
        #         max_range = ranges[i]
        #         max_range_index = i

        max_range_index = (start_i+end_i)/2
        return max_range_index

    def lidar_callback(self, data):
        """ Process each LiDAR scan as per the Follow Gap algorithm & publish an AckermannDriveStamped Message
        """
        ranges = list(data.ranges)
        # print(type(ranges))
        proc_ranges = self.preprocess_lidar(ranges)
        # print(proc_ranges)
        #Find closest point to LiDAR
        min_range = proc_ranges[0]
        min_index = 0
        for i in range(len(proc_ranges)):
            if proc_ranges[i] < min_range:
                min_range = proc_ranges[i]
                min_index = i
        # print(min_range)
        #Eliminate all points inside 'bubble' (set them to zero) 
        # for i in range(len(proc_ranges)):
        #     angle = abs(min_index - i)*data.angle_increment
        #     pos = math.sqrt(proc_ranges[min_index]**2 + proc_ranges[i]**2 - 2*proc_ranges[min_index]*proc_ranges[i]*math.cos(angle))
        #     if (pos < rb):
        #         proc_ranges[i] = 0.0

        if rb < min_range:
            angle = math.asin(rb/min_range)
            elim_index_range = int(angle/data.angle_increment)
        elif rb >= min_range:
            # angle = data.angle_increment
            elim_index_range = min(min_index, 1080/2-1)
        # print(elim_index_range)
        for i in range(min_index - elim_index_range, min_index + elim_index_range):
            proc_ranges[i] = 0.0

        # p_list = []
        # for i in range(4*105,1080-4*105):
        #     if proc_ranges[i] == look_ahead_distance:
        #         p_list.append(i)
        # print(p_list)
        #Find max length gap
        i_index, f_index = self.find_max_gap(proc_ranges)

        # print(math.degrees(i_index*data.angle_increment), math.degrees(f_index*data.angle_increment))

        #Find the best point in the gap 
        angle_steer_index = self.find_best_point(i_index, f_index, proc_ranges) #max_gap_ranges)
        angle_steer = angle_steer_index*data.angle_increment - math.pi

        # print(angle_steer_index)

        # if (angle_steer > math.radians(max_steering_angle)):
        #     angle_steer = math.radians(max_steering_angle)
        # if (angle_steer < math.radians(-max_steering_angle)):
        #     angle_steer = math.radians(-max_steering_angle)

        # print(math.degrees(angle_steer))

        # velocity
        if abs(math.degrees(angle_steer)) < 10:
            vel = 5.0
        elif abs(math.degrees(angle_steer)) < 20:
            vel = 1.35
        else:
            vel = 0.5

        ##3,1.5,0.5;   3,2.5,1.5;   4,3,2;  4,3,2.5;    4,3.0,1.0;  4,2.5,0.5;  4.5,1,0.5,lad=1.35;
        ##4.5,1,0.5,lad=1.5;  5.5,0.75,0.5,lad=1.5;     5.86,1.25,0.1,lad=1.5-7,8;

        ##5.5,1.25,0.1,lad=1.8;     5.5,1.25,0.3,lad=1.8-8;     5.5,1.35,0.5,lad=1.8->15;

        #Publish Drive message
        drive_msg = AckermannDriveStamped()
        drive_msg.header.stamp = rospy.Time.now()
        drive_msg.header.frame_id = "base_link"
        drive_msg.drive.steering_angle = angle_steer
        drive_msg.drive.speed = vel #velocity
        self.drive_pub.publish(drive_msg)

def main(args):
    rospy.init_node("FollowGap_node", anonymous=True)
    rfgs = reactive_follow_gap()
    rospy.sleep(0.1)
    rospy.spin()

if __name__ == '__main__':
    main(sys.argv)